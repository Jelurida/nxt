This is a stable maintenance release with minor bugfixes only.

Table trimming improvements. Fixed decryption of some Marketplace messages.

Added a contrib/nxt-ramdisk.sh script to enable running Nxt with the database
stored in memory on tmpfs. Run the script with no arguments for more help.

Support NXT_JVM_OPTS and NXT_PID_FILE environment variables in run.sh and
stop.sh scripts.

Updated Jetty to version 9.3.30 and Bouncy Castle to 1.70.

